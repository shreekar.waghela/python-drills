def is_prime(n):
    """
    Check whether a number is prime or not
    """
    if n > 1:
        for i in range(2, n):
            if n % i == 0:
                return False
        return True


def n_digit_primes(digit = 2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    prime_numbers = []

    if digit == 1:
        prime_numbers.append(2)
        for j in range(3, 10):
            if is_prime(j):
                prime_numbers.append(j)
                
    elif digit == 2:
        for k in range(10,100):
            if is_prime(k):
                prime_numbers.append(k)
    
    return prime_numbers
            
