def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    array = []
    for i in range(start, end, step):
        array.append(i)
    return array


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    array = []
    while (True):
        array.append(start)

        start += step
        
        if step < 0 and start <= end:
            break
        elif step > 0 and start >= end:
            break
    return array


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    array = []
    for i in range(10, 100):
        for n in range(2, i):
            if (i%n == 0):
                break
        else:
            array.append(i)
    return array