"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    f = open(path)
    string = f.read()
    f.close()
    return string


def write_to_file(path, s):
    f = open(path, 'w')
    f.write(s)
    f.close()


def append_to_file(path, s):
    f = open(path, 'a')
    f.write(s)
    f.close()


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    f = open(file_path, 'w')
    for i in range(1,n+1):
        f.write(f'{i},{i**2}\n')
    f.close()
