def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    result = []

    selector_value = selector[1:]

    def search(obj, selector_value):

        for key in obj.keys():
            if key == "attrs":
                for key in obj["attrs"]:
                    if key == "class":
                        if obj["attrs"][key] == selector_value:
                            result.append(obj)
                    elif key == "id":
                        if obj["attrs"][key] == selector_value:
                            result.append(obj)
            elif key == "children":
                for objects in obj[key]:
                    search(objects, selector_value)
    
    search(html_dict, selector_value)
    return result